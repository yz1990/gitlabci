//
//  ViewController.m
//  StaticLibMade
//
//  Created by yangzhen10 on 2019/3/13.
//  Copyright © 2019 yangzhen. All rights reserved.
//

#import "ViewController.h"
#import "LibA.h"
#import "LibX.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[LibA new] methodA:@"HH"];
    // Do any additional setup after loading the view, typically from a nib.
}


@end
