//
//  LibA.m
//  StaticLibMade
//
//  Created by yangzhen10 on 2019/3/13.
//  Copyright © 2019 yangzhen. All rights reserved.
//

#import "LibA.h"
#import "LibX.h"

@implementation LibA
- (void)methodA:(NSString *)test
{
    NSLog(@"****%@",test);
    [[LibX new] methodX:test];
}
@end
