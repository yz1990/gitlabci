//
//  LibA.h
//  StaticLibMade
//
//  Created by yangzhen10 on 2019/3/13.
//  Copyright © 2019 yangzhen. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LibA : NSObject
- (void)methodA:(NSString *)test;
@end

NS_ASSUME_NONNULL_END
