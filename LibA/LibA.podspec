Pod::Spec.new do |spec|
    spec.name         = "LibA"
    spec.homepage     = "https://github.com/MikoOne/"
    spec.license      = 'Private'
    spec.author       = { "yangzhen10" => "yangzhen10@staff.weibo.com" }
    spec.version      = "0.0.2"
    spec.summary      = spec.name
    spec.source       = { :path => "." }
    spec.source_files  = "*.h"
        spec.source_files  = "*{.h,.m}"
#    spec.vendored_libraries = 'libLibA.a'
    spec.dependency 'LibX'
end
